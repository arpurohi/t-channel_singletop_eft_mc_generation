from CRABClient.UserUtilities import config
config = config()

config.General.requestName     = 'ST_CPV_EFT_729Reweight_13TeV_amcatNLO_UL18_NANOAOD'
config.General.workArea        = 'crab'
config.General.transferOutputs = True
config.General.transferLogs    = False
config.JobType.maxMemoryMB = 3500
config.JobType.pluginName  = 'Analysis'
config.JobType.psetName    = 'step7NANOAOD_V2_cfg.py'
config.Data.inputDataset = "/ST_CPV_EFT_Reweight_TuneCP5_PSweights_13TeV_amcatnloFXFX_pythia8/arpurohi-RunIISummer20UL18MiniAOD-106X_upgrade2018_realistic_v13_L1v1-v2-07bb2832fd9cf08ee8da01c42829422a/USER"
config.Data.inputDBS = "phys03"
config.Data.splitting   = 'FileBased'
config.Data.unitsPerJob = 1
config.Data.totalUnits  = -1
config.Data.outLFNDirBase = '/store/user/arpurohi/'
config.Data.ignoreLocality = True
config.JobType.outputFiles  = ['step7_NANOAOD.root']
config.Data.publication = True
config.Data.outputDatasetTag     = 'RunIISummer20UL18MiniAOD-106X_upgrade2018_realistic_v13_L1v1-v2_NANOAOD'
config.JobType.allowUndistributedCMSSW=True
config.section_("Site")
config.Site.storageSite = 'T2_US_Purdue'
config.Site.whitelist = ['T2_US_Caltech','T2_US_Florida','T2_US_MIT','T2_US_Nebraska','T2_US_Vanderbilt','T2_US_Wisconsin', 'T2_US_UCSD', 'T2_CH_CERN', 'T3_US_FNALLPC', 'T2_US_Purdue']
# this is needed in order to prevent jobs overflowing to blacklisted sites
config.section_("Debug")
config.Debug.extraJDL = ['+CMS_ALLOW_OVERFLOW=False']
