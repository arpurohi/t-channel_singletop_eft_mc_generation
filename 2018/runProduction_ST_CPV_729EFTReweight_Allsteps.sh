#!/bin/bash

echo "================= CMSRUN starting jobNum=$1 ====================" | tee -a job.log
ls -ltrh 

source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700

BASE=$PWD
MYCMSSW=CMSSW_10_6_21


echo "================= CMSRUN setting up $MYCMSSW ===================="| tee -a job.log
if [ -r $MYCMSSW/src ] ; then 
    echo release $MYCMSSW already exists
else
    scram p CMSSW $MYCMSSW
fi
cd $MYCMSSW/src
eval `scram runtime -sh`
scram b 
cd $BASE

echo "================= CMSRUN starting Step 1 ====================" | tee -a job.log
cmsRun ST_EFT_CPV_cfg.py  jobNum=$1

MYCMSSW=CMSSW_10_6_17_patch1


echo "================= CMSRUN setting up $MYCMSSW ===================="| tee -a job.log
if [ -r $MYCMSSW/src ] ; then 
    echo release $MYCMSSW already exists
else
    scram p CMSSW $MYCMSSW
fi
cd $MYCMSSW/src
eval `scram runtime -sh`
scram b 
cd $BASE

echo "================= CMSRUN starting Step 2 ====================" | tee -a job.log
cmsRun step2_SIM_cfg.py


CLEAN=step1_GEN.root
echo "--> cleaning up $CLEAN"
rm -v $CLEAN

CLEAN=step1_GEN_inLHE.root
echo "--> cleaning up $CLEAN"
rm -v $CLEAN

echo "================= CMSRUN starting Step 3 ====================" | tee -a job.log
cmsRun -e -j FrameworkJobReport.xml step3_DIGI_cfg.py

CLEAN=step2_SIM.root
echo "--> cleaning up $CLEAN"
rm -v $CLEAN

export SCRAM_ARCH=slc7_amd64_gcc700
MYCMSSW=CMSSW_10_2_16_UL
echo "================= CMSRUN setting up $MYCMSSW ===================="| tee -a job.log
if [ -r $MYCMSSW/src ] ; then 
    echo release $MYCMSSW already exists
else
    scram p CMSSW $MYCMSSW
fi
cd $MYCMSSW/src
eval `scram runtime -sh`
scram b 
cd $BASE

echo "================= CMSRUN starting Step 4 ====================" | tee -a job.log
cmsRun -j step4.log step4_HLT_cfg.py 

ls -lrth

CLEAN=step3_DIGI.root
echo "--> cleaning up $CLEAN"
rm -v $CLEAN

export SCRAM_ARCH=slc7_amd64_gcc700
MYCMSSW=CMSSW_10_6_17_patch1
echo "================= CMSRUN setting up $MYCMSSW ===================="| tee -a job.log
if [ -r $MYCMSSW/src ] ; then 
    echo release $MYCMSSW already exists
else
    scram p CMSSW $MYCMSSW
fi
cd $MYCMSSW/src
eval `scram runtime -sh`
scram b 
cd $BASE

export HOME=${BASE}

echo "================= CMSRUN starting Step 5 ====================" | tee -a job.log

cmsRun -j step5.log step5_AOD_cfg.py

CLEAN=step4_HLT.root
echo "--> cleaning up $CLEAN"
rm -v $CLEAN

MYCMSSW=CMSSW_10_6_20

echo "================= CMSRUN setting up $MYCMSSW ===================="| tee -a job.log
if [ -r $MYCMSSW/src ] ; then 
    echo release $MYCMSSW already exists
else
    scram p CMSSW $MYCMSSW
fi
cd $MYCMSSW/src
eval `scram runtime -sh`
scram b 
cd $BASE


echo "================= CMSRUN starting Step 6 ====================" | tee -a job.log
cmsRun  -j FrameworkJobReport.xml step6_MINIAOD_cfg.py

CLEAN=step5_AOD.root
echo "--> cleaning up $CLEAN"
rm -v $CLEAN

#MYCMSSW=CMSSW_10_6_17_patch1
#echo "================= CMSRUN setting up $MYCMSSW ===================="| tee -a job.log
#if [ -r $MYCMSSW/src ] ; then 
#    echo release $MYCMSSW already exists
#else
#    scram p CMSSW $MYCMSSW
#fi
#cd $MYCMSSW/src
#eval `scram runtime -sh`
#scram b 
#cd $BASE

#echo "================= CMSRUN starting Step 7 ====================" | tee -a job.log
#cmsRun -e -j FrameworkJobReport.xml step7_cfg.py
#CLEAN=step6_MINIAOD.root
#echo "--> cleaning up $CLEAN"
#rm -v $CLEAN
echo "================= CMSRUN finished ====================" | tee -a job.log

