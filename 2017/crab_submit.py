from CRABClient.UserUtilities import config
config = config()

config.General.requestName     = 'ST_CPV_EFT_729Reweight_13TeV_amcatNLO_UL18_GENToMINIAOD_Aug30_2MEvents_AllWorld'
config.General.workArea        = 'crab'
config.General.transferOutputs = True
config.General.transferLogs    = False

config.JobType.pluginName  = 'PrivateMC'
config.JobType.psetName    = 'step6_MINIAOD_fake_cfg.py'
config.JobType.maxMemoryMB = 3500
config.JobType.inputFiles  = ['runProduction_ST_CPV_729EFTReweight_Allsteps.sh','/eos/lyoeos.in2p3.fr/home/apurohit/SingleTop_Sample_Generation/Jan2024/Production_dir/t_channel_EFT_atLO_nomadspin_reweight_729_init2_slc7_amd64_gcc700_CMSSW_10_6_19_tarball.tar.xz','ST_EFT_CPV_cfg.py','step2_SIM_cfg.py','step3_DIGI_cfg.py','step4_HLT_cfg.py','step5_AOD_cfg.py','step6_MINIAOD_cfg.py']

config.JobType.scriptExe   ='runProduction_ST_CPV_729EFTReweight_Allsteps.sh'

config.Data.splitting   = 'EventBased'
config.Data.unitsPerJob = 500
config.Data.totalUnits  = 2000000
config.Data.outLFNDirBase = '/store/user/arpurohi/'
#config.Data.ignoreLocality = True                                                                                                                                                                      
config.JobType.outputFiles  = ['step6_MINIAOD.root']
config.Data.publication = True
config.Data.outputPrimaryDataset = 'ST_CPV_EFT_Reweight_TuneCP5_PSweights_13TeV_amcatnloFXFX_pythia8'
config.Data.outputDatasetTag     = 'RunIISummer20UL18MiniAOD-106X_upgrade2018_realistic_v13_L1v1-v2'
config.JobType.allowUndistributedCMSSW=True
config.section_("Site")
config.Site.storageSite = 'T3_FR_IPNL'
#config.Site.storageSite = 'T2_US_Purdue'
#config.Site.whitelist = ['T2_US_Purdue']                                                                                                                                                               
# this is needed in order to prevent jobs overflowing to blacklisted sites                                                                                                                              
config.section_("Debug")
config.Debug.extraJDL = ['+CMS_ALLOW_OVERFLOW=False']
