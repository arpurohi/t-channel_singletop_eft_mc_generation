BASE=$PWD
MYCMSSW=CMSSW_10_6_26
echo "================= CMSRUN setting up $MYCMSSW ===================="| tee -a job.log                                                                                                               
if [ -r $MYCMSSW/src ] ; then                                                                                                                                                                           
   echo release $MYCMSSW already exists                                                                                                                                                                
else                                                                                                                                                                                                    
   scram p CMSSW $MYCMSSW                                                                                                                                                                              
fi                                                                                                                                                                                                      
cd $MYCMSSW/src                                                                                                                                                                                         
eval `scram runtime -sh`                                                                                                                                                                                
scram b                                                                                                                                                                                                 
cd $BASE                                                                                                                                                                                                
echo "================= CMSRUN starting Step 7 ====================" | tee -a job.log                                                                                                                   
cmsRun -e -j FrameworkJobReport.xml step7_NANOAOD_cfg.py                                                                                                                               
#CLEAN=step6_MINIAOD.root                                                                                                                                                                               
#echo "--> cleaning up $CLEAN"                                                                                                                                                                          
#rm -v $CLEAN                                                                                                                                                                                           
 
echo "================= CMSRUN finished ====================" | tee -a job.log

