# Sample Generation for Single Top EFT samples with Madgraph reweighting technique

This project shows how to produce MC samples for the t-channel Single top EFT samples for Ctw, Cbw and Cphitb


You can first create a CMSSW area,

cmsrel CMSSW_10_6_17_patch1

Then go to the src to load the environment required to submit the crab job.

cd CMSSW_10_6_17_patch1/src
cmsenv

cd ../..

Before running the crab jobs please change the parameters in the configuration file. Update the path where you want to store the output files etc..
crab submit crab_submit.py   
